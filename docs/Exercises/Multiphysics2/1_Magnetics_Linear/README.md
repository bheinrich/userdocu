# HW1: Linear Magentic


### Description
We consider the axi-symmetric geometry (2D model) of a ring-shaped permanent magnet as depicted in the sketch.
The inner radius of the ring $r_1$ is 5 mm, the outer radius $r_2$ is 10 mm, and the height of the ring $l_1$ is 8 mm. 
The size of the simulation domain should be chosen sufficiently large, such that the solution is not significantly influenced by the boundary conditions at the outer edge of the domain.
The ring-shaped permanent magnet is assumed to show a perfect magnetization $M$ of 1.2 T in z-direction. 
The core of the ring is either air, copper, aluminum or iron. 


![sketch](sketch.png){: style="width:150px"}

Use the material air for the domain of the permanent magnet. In this homework we assume linear material behavior. For the air domain and the magnet the free space magnetic permeability $\mu_0$ can be assumed.

---------------------------

### Tasks

1. Create a regular hexaedral mesh using Trelis. **(2 Points)**
2. Create input-xml files for the required CFS++ simulations. **(2 Points)**
3. Document your results by answering the questions below. **(8 Points)**
4. Answer the theoretical question. **(3 Points)**

### Hints

- Use the provided templates: simulation input [`magnet.xml`](magnet.xml) and material file [`mat.xml`](mat.xml)
- Adapt the Trelis input [`geometry-ue.jou`](geometry-ue.jou) to include the ring.
- The complete workflow for the example is included in the shell script [`run.sh`](run.sh), uncomment the necessary lines (remove leading `#`) to execute cfs and trelis (`#trelis ...` and `#cfs ...` and `#sed ...`)
- A distance $d \approx 2r_2$ from the ring to the outer boundary should be sufficient.

### Submission
Submit all your input files (`*.jou`, `*.xml`) as well as a concise PDF report of your results.

---------------------------

### Modeling Assumptions 
What are the modeling assumptions in the analyses? Consider the used PDE (and the assumptions necessary to derive it) as well as material behavior and boundary conditions. How is the free
space magnetic permeability defined? **(2 Points)**

### Material Behavior
What is the difference between the materials iron, copper and aluminum in magnetics? **(1 Point)**

### Material Influence
Perform two different linear analyses using iron and air for the material in the core region.

- Plot the magnetic vector potential **(1 Points)**
- Plot the magnetic field lines and the vectors of the magnetic flux density **(2 Points)**

What are the differences between iron and air? **(1 Point)**
Now use the materials aluminum and copper for the core and compare them.
Is there a difference in the result? Give a short explanation if there is one or not? **(1 Point)**

### Theoretical part
Describe the PDE for solving electromagnetics, including the eddy current parts. How is the magnetic vector potential introduced?

---------------------------

You can download the templates linked on this page individually by _right click --> save target as_.
Alternativey, head over to the [git repository](https://gitlab.com/openCFS/userdocu/-/tree/master/docs/Exercises/Multiphysics2) and use the _download button_ on the top right: 
Here is a direct link to the [__zip archive__](https://gitlab.com/openCFS/userdocu/-/archive/master/HW1.zip?path=docs/Exercises/Multiphysics2/1_Magnetics_Linear).
