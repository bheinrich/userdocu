## Electric flow - heat conduction coupling
This coupling allows the simulation of Joul's heating by current flow in structures and its resulting temperature distribution.

### Coupling conditions
We consider the following coupling terms: (1) Joule's (heating) loss
\begin{equation}
\dot q(\mathbf{x}, t) = \mathbf{J} \cdot \mathbf{E} 
= \gamma\,\mathbf{E} \cdot \mathbf{E}
= \gamma\, \nabla {\phi}\cdot \nabla {\phi}
\end{equation}
with $\mathbf J$ the current density, $\mathbf E$ the electric field intensity as well as $\phi$ the electric potential and (2) the dependecy of the electric conductivity $\gamma$ on temperature $T$
\begin{equation}
\gamma = \gamma(T)
\end{equation}

### Iterative coupling schemes
With this coupling terms, the following coupled system of PDEs for the electric flow and heat conduction is solved
\begin{equation}
- \nabla \gamma(T)\nabla {\phi} = 0\,;  \ \ \ \ \
\phi = \phi_\mathrm{e} \text{ on }\Gamma\mathrm{e}\,; \ \ \ \
\mathbf{J}\cdot\mathbf{n} = -\gamma(T)\nabla {\phi} \cdot\mathbf{n} = J_\mathrm{n}  \text{ on }\Gamma\mathrm{n}
\end{equation}

\begin{eqnarray*}
\rho c_{\mathrm m}(T) \frac{\partial T(\mathbf{x},t)}{\partial t} - \nabla {\left( k(T) \nabla{T(\mathbf{x},t)} \right)}  
 &=& \big( \nabla{\phi} \big) \cdot \gamma(T) \nabla{\phi} \\
T(\mathbf{x},t) &=& T\mathrm{e}(\mathbf{x},t)\ \ \ \ \ \  \text{on }\Gamma\mathrm{e}\\
\mathbf{q}\cdot\mathbf{n} &=& q_\mathrm{n} \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \,\text{on }\Gamma_\mathrm{n} \\
\mathbf{q}\cdot\mathbf{n} &=& \alpha \left(T - T\mathrm{r}\right) \ \text{on }\Gamma_\mathrm{tr} 
\end{eqnarray*}

For details towards boundary conditions for electirc flow PDE [see](../../Singlefield/ElectricFlowPDE/README.md) and for heat condution PDE [see](../../Singlefield/HeatPDE/README.md). 

The PDEs are sequentially solved starting with the electirc flow PDE, computing Joule's lossed and then use them when solving the heat condution PDE.

<img src="FlowChartElecHeat.png" width="250">

* **Nonlinearity in heat conduction:**Fix-point method with stopping criterion

\begin{equation}
\frac{|| \mathbf{T}_{i+1} - \mathbf{T}_i ||_2}{|| 
\mathbf{RHS} ||_2} \le \varepsilon
\approx 10^{-3}
\end{equation}

* **Stopping criterion for outer iteration:** relative change of Joule losses

\begin{equation}
\frac{|| \mathbf{f}_{k+1} - \mathbf{f}_k ||_2}{|| \mathbf{f}_{k+1} ||_2} \le \varepsilon
\approx 10^{-3}
\end{equation}


### Analysis Types

Depending on the temporal setting, the coupled PDEs can be solved for the following analysis types:

* *Static-*case ($\partial / \partial t = 0$)
```
<analysis>
	<static>
	</static>
</analysis>
```
* *Transient-*case ($\partial / \partial t (\cdot) \neq 0$) 
```
<analysis>
  <transient>
		<numSteps>100</numSteps>
		<deltaT>1e-3</deltaT>
  </transient>
</analysis>
```

### Single PDEs

Within the command ```<pdeList>``` we define the induvidual PDEs with their boundary conditions, etc.
```
<pdeList>
	<elecConduction systemId="elecPDE">
		...
	</elecConduction>
	...
	<heatConduction  systemId="heatPDE">
		...
	</heatConduction>
</pdeList>
```


### Defining the coupling
* **Coupling terms:** The dependency of the electric conductivity $\gamma$ on the tempertaure is defined for each region within the electric flow PDE itself, e.g.
```
	<elecConduction systemId="elecPDE">
		<regionList>
        		<region name="Vol_poly_A1" matDependIds="cond"/>
        		.....           
		</regionList>

		<matDependencyList>
			<elecConductivity id="cond">
				<coupling pdeName="heatConduction">
					<quantity name="heatTemperature"/>
				</coupling>
			</elecConductivity>
		</matDependencyList>    
		.....
	</elecConduction systemId="elecPDE">	
```
In addition, the coupling term via Joule's losses are defined in the heat conduction PDE for each region in ```<bcsAndLoads>```, e.g.
```
	<heatConduction  systemId="heatPDE"
		......
		<bcsAndLoads>
			....

			<elecPowerDensity name="Vol_poly_A1">
				<coupling pdeName="elecConduction">
					<quantity name="elecPowerDensity"/>
					</coupling>           
			</elecPowerDensity>   
			.....
		</bcsAndLoads>                    	
			.....
	</heatConduction>
```
* **Iterative scheme:** In order to achieve a converges solution, we define an interative coupling with corresponding stopping criterion within the ```<couplingList>```:

```
        <couplingList>
            <iterative>
                <convergence logging="yes" maxNumIters="20" stopOnDivergence="yes">
                   <quantity name="elecPower" value="1e-3" normType="rel"/>
                </convergence>
            </iterative>
        </couplingList>
```
 
### Material and postprocessing results

For the definition of the individual material as well as the postprocessing results please refer to the individual description of the PDE covered in the *Singlefield*-section. Here is an example of a material definition including nonlineartis for the heat capacity, heat conduction and temperature dependency of the electric conductivity
```
<material name="Si8res">
    <heatConduction>
      <density>
        <linear>
          <real>2330</real>
        </linear>
      </density>
      <heatCapacity>
        <linear>
          <real>
            733
          </real>
        </linear>
        <nonlinear>
          <dependency>temperature</dependency>
          <approxType>LinInterpolate</approxType>
          <measAccuracy>0.05</measAccuracy>
          <maxApproxVal>871.049</maxApproxVal>
          <dataName>Cap_OTS_Si_8.fnc</dataName>
        </nonlinear>
      </heatCapacity>
      <heatConductivity>
        <linear>
          <isotropic>
            <real>
            155.761
          </real>
          </isotropic>
        </linear>
        <nonlinear>
          <isotropic>
            <dependency>temperature</dependency>
            <approxType>LinInterpolate</approxType>
            <measAccuracy>0.05</measAccuracy>
            <maxApproxVal>283.269</maxApproxVal>
            <dataName>Cond_OTS_Si_8.fnc</dataName>
          </isotropic>
        </nonlinear>
      </heatConductivity>
    </heatConduction>
    <elecConduction>
      <electricConductivity>
        <linear>
          <isotropic>
            <real> 1.233E+05 </real>
          </isotropic>
        </linear>
        <nonlinear>
          <isotropic>
            <dependency>temperature</dependency>
            <approxType>LinInterpolate</approxType>
            <measAccuracy>0.05</measAccuracy>
            <maxApproxVal>1.63209349E+05</maxApproxVal>
            <dataName>ElecCond_OTS_Si_8.fnc</dataName>
          </isotropic>
        </nonlinear>
      </electricConductivity>
    </elecConduction>
</material>
```

