# Defining own coordinate systems

In openCFS you can define your own coordinate systems.

```
<coordSysList>
  <cylindric id="cyl1">
    <origin x="0.0" y="0.0" z="0.0"/>
    <zAxis x="0.0" y="1" z="0.0"/>
    <rAxis x="1" y="0.0" z="0.0"/>
  </cylindric>
</coordSysList>
```
This can be especially useful if you need cylindric coordinates inside a cartesian coordinate system. E.g. defining the current direction in a coil in a 3d-domain.

<details>
  <summary>See a typical xml-input which uses a custom coordinate system</summary>
```
<cfsSimulation> 
  <fileFormats>
    <input>
      <hdf5/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
      <text id="txt2" fileCollect="timeFreq"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="3d" printGridInfo="yes">
    <variableList>
      <var name="depth" value="50e-3*2"/>
      <var name="radius" value="5e-3"/>
    </variableList>
    <regionList>
      <region name="air"  material="air"/>
      <region name="coil" material="air"/>
      <region name="core" material="air"/>
    </regionList>
    <!-- Cylindric coordinate system for coil current -->
    <coordSysList>
      <cylindric id="mid"> 
        <origin x="0" y="0" z="0"/>
        <zAxis z="1" />
        <rAxis x="1" />
      </cylindric>
    </coordSysList>
  </domain>
  
  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>
    <pdeList>
      <magneticEdge systemId="default">
        <regionList>
          <region name="air"/>
          <region name="coil"/>
          <region name="core"/>
        </regionList>
        <bcsAndLoads>
          <fluxParallel name="x-inner"/>
          <fluxParallel name="x-outer"/>
          <fluxParallel name="y-inner"/>
          <fluxParallel name="z-outer"/>
        </bcsAndLoads>
        <coilList>
          <coil id="myCoil">
            <source type="current" value="1"/>
            <part>
              <regionList>
                <region name="coil"/>
              </regionList>
              <direction>
                <analytic coordSysId="mid">
                  <comp dof="phi" value="1"/>
                </analytic>
              </direction>
              <wireCrossSection area="2e-6"/>
              <resistance value="0"/> 
            </part>
          </coil>
        </coilList>
        <storeResults>
          <elemResult type="magPotential">
            <allRegions/>
          </elemResult>
          <elemResult type="magFluxDensity">
            <allRegions/>
            <elemList>
              <elems name="hist" outputIds="txt2"/>
            </elemList>
          </elemResult>
          <regionResult type="magEnergy">
            <allRegions outputIds="txt"/>
          </regionResult>
        </storeResults>
      </magneticEdge>
    </pdeList>
  </sequenceStep>
</cfsSimulation>
```
</details>

The xml-scheme from above is taken from the [testsuite]([www.google.at](https://gitlab.com/openCFS/Testsuite/-/tree/master/TESTSUIT/Singlefield/Magnetics/CylindricCoilEdge))