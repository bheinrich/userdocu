# Stokes Boundary Layer

##Problem Discription

All files can be [downloaded here](linFlow.zip)

In this tutorial we will have a look at the [stokes problem](https://en.wikipedia.org/wiki/Stokes_problem), where we use the [LinFlow-PDE](../../PDEExplanations/Singlefield/LinFlowPDE/README.md) to solve this problem. The bottom is excited with a velocity which creates a flow inside the fluid. Depending on the frequency the boundary layer changes.

Here a good illustration from wikipedia:

![pic from wiki](Stokes_boundary_layer.gif)

## Tutorial Suggestions

* Run the simulation
* Try to visualize the velocity field in paraview ([How to visualize field results](../../PostProcessing/ParaView/basics/#visualizing-field-results) and see how the field oszilates for different frequencies ([How to animate harmonic results](../../PostProcessing/ParaView/basics/#animate-harmonic-results-in-paraview))
* Try to visualize the velocity over the boundary layer thickness for the different frequency steps, [using python](../../PostProcessing/PythonPostProcessing/README.md).

## Coupling to other fields

You can couple the linFlow-PDE to various fields. For example's look into our testsuite:

* [Coupling to the Acoustic field](https://gitlab.com/openCFS/Testsuite/-/tree/master/TESTSUIT/Coupledfield/LinFlowAcou)
* [Coupling to the Mechanic field](https://gitlab.com/openCFS/Testsuite/-/tree/master/TESTSUIT/Coupledfield/LinFlowMech)
* [Coupling to the Thermal field](https://gitlab.com/openCFS/Testsuite/-/tree/master/TESTSUIT/Coupledfield/LinFlowHeat)
