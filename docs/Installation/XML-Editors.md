# XML Editors for openCFS Input Files

The XML based simulation input files for openCFS can be written in any text editor.
However, specialised XML editors reading the XML-schema will greatly simplify input definition since they typically offer features like [auto-completion](../XMLExplanations/README.md#xml-autocompleting) and [schema validation](../XMLExplanations/README.md#scheme-suggestions).

Below you find a view choices:

* [Ecplise IDE (with xml plugin)](#eclipse-ide-with-xml-plugin)
* [Visual Studio Code](#visual-studio-code)
* [oXygen](#oxygen)

For an instruction on how to edit an xml file and to find out more about the structure of the XML scheme, see the [XML Explantions](../XMLExplanations/README.md) page.


## eclipse IDE (with xml plugin)

[eclipse IDE](https://www.eclipse.org/ide/) is a powerful IDE and supports XML auto-completion and shema validation.

First install eclipse, e.g. on Ubuntu Linux via the package manager
```shell
sudo apt install eclipse
```
Then open eclipse and go to `Help -> Eclipse Marketplace` and search for *Eclipse XML Editors* and install it.
Then restart eclipse.

## Visual Studio Code

[Visual Studio Code](https://code.visualstudio.com/) has many plugins for XML files.
Follow the official docs on [installation](https://code.visualstudio.com/docs/setup/setup-overview)
and [managing extensions](https://code.visualstudio.com/docs/editor/extension-marketplace).
The [XML extension from redhat](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml) offers XML validation and autocompletion with schema support.

## oXygen

The commercial [oXygem XML Editor](https://www.oxygenxml.com/) supports auto-completion and schema validation (and much more).
It is available for all common platforms and they offer free trial versions.
